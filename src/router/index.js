import Vue from "vue";
import Router from "vue-router";
import mainPage from "../components/mainPage";
import formationPage from "@/components/formationPage";
import competencePage from "@/components/competencePage";
import xpProPage from "@/components/xpProPage";
import personCIPage from "@/components/personCIPage";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "main",
      component: mainPage
    },
    {
      path: "/formation",
      name: "formation",
      component: formationPage
    },
    {
      path: "/competences",
      name: "competences",
      component: competencePage
    },
    {
      path: "/xpPro",
      name: "xpPro",
      component: xpProPage
    },
    {
      path: "/personCI",
      name: "personCI",
      component: personCIPage
    }
  ]
});
